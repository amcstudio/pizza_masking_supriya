'use strict';

~(function() {
    var 
        bgExit = document.getElementById('bgExit'),
        ad = document.getElementById('mainContent');

    window.init = function() {
        var tl = gsap.timeline({ repeat:-1});

        tl.addLabel('frameOne');
        tl.to(['.pizzaOutline', '.pizzaMask'], 7, { rotation: 360, svgOrigin: '61 61', ease: 'none' },'frameOne ')
        tl.to('.whole', 7, { rotation: -45, svgOrigin: '61 61', ease: 'none' }, 0)
        
    }

    bgExit.addEventListener("click", function(e) {
        e.preventDefault();
        window.open(window.clickTag);
    });

})();