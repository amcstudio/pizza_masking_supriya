var gulp = require('gulp'),
browserSync = require('browser-sync');


module.exports = {
    applySync : function (){
        browserSync({
           injectChanges: true,
            server: {
              baseDir: "./src"
            }
          });
    },
    bsReload : function (done){
        browserSync.reload();
        done();
    }

}