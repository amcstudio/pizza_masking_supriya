var gulp = require('gulp'), 
del = require('del');


module.exports = function () {
    return del(['./_publishZip/', './deploy/**/.*', './deploy/**/_*', './deploy/**/*.db'])
}